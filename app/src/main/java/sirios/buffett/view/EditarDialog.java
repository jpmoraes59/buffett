package sirios.buffett.view;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.regex.Pattern;

import sirios.buffett.R;
import sirios.buffett.controller.NetworkUtils;
import sirios.buffett.model.Tasks;

public class EditarDialog extends Dialog {

    private EditText name;
    private EditText detais;
    private EditText hora;
    private EditText id;

    private Tasks task;

    public EditarDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.adicionar_dialog);

        Button buttonAdicionar = (Button) findViewById(R.id.buttonAdcionar);
        buttonAdicionar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                adciona();
            }
        });

        Button buttonCancelar = (Button) findViewById(R.id.buttonCancelar);
        buttonCancelar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                cancelar();
            }
        });

        hora = (EditText) findViewById(R.id.textHora);
        hora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        hora.setText( selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

    }



    void adciona(){
        task = new Tasks();

        name = (EditText) findViewById(R.id.textName);
        detais = (EditText) findViewById(R.id.textDetails);
        id = (EditText) findViewById(R.id.textID);

        task.setName(name.getText().toString());
        task.setDetails(detais.getText().toString());
        task.setHours(hora.getText().toString());
        System.out.println(task.getId());

        SharedPreferences prefs = getContext().getSharedPreferences("ID", getContext().MODE_PRIVATE);
        int restoredText = prefs.getInt("id", 0);


        task.setId(restoredText);

        Boolean b = Pattern.matches ("^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$", hora.getText());
        System.out.println(b);


//        Log.i("name", name.getText().toString());
        System.out.println("----------------------------------------------"+ task.getId());
        if(name.getText().toString().matches("") || detais.getText().toString().matches("")
                || hora.getText().toString().matches("")){
            mostraToast("Campos vazios");
        }else if (b == true){
            mostraToast("Horas invalida");
        }else{
            NetworkUtils.Editar(task);
            mostraToast(task.getName().toString() + " Adicionado");
            Context context = getContext();
            Intent intent = new Intent(context, MainActivity.class);
            context.startActivity(intent);


        }
    }

    void cancelar(){
        this.dismiss();
    }

    void mostraToast(String texto){
        Toast.makeText(getContext(), texto, Toast.LENGTH_SHORT).show();


    }


}

