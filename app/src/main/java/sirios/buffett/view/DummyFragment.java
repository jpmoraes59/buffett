package sirios.buffett.view;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import sirios.buffett.R;
import sirios.buffett.controller.Utils;
import sirios.buffett.model.Tasks;

/*
 * modelo de fragmento, assume o layout  que os fragments(telas chamadas palas abas) devem ter
 * */
public class DummyFragment extends Fragment  implements SwipeRefreshLayout.OnRefreshListener {

    private View view;
    private View swipe;
    private String title;//String para titulo da tab
    private ProgressDialog load;
    SwipeRefreshLayout mSwipeRefreshLayout;



    private static RecyclerView recyclerView;

    public DummyFragment() {
    }

    @SuppressLint("ValidFragment")
    public DummyFragment(String title) {
        this.title = title;//Seta o titulo da tab
    }

    @Nullable
    @Override //seta o o layot do fragmento
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.modelo_fragment, container, false);
        swipe = inflater.inflate(R.layout.modelo_fragment, container, false);

        GetJson download = new GetJson();
        boolean conectado;
        ConnectivityManager conectivtyManager = (ConnectivityManager) getContext().getSystemService(getContext().CONNECTIVITY_SERVICE);
        if (conectivtyManager.getActiveNetworkInfo() != null
                && conectivtyManager.getActiveNetworkInfo().isAvailable()
                && conectivtyManager.getActiveNetworkInfo().isConnected()) {
            conectado = true;
        } else {
            conectado = false;
        }
        System.out.println("------------------------------"+conectado);
            download.execute();


        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        return view;

    }

    @Override
    public void onRefresh() {
        refresh download = new refresh();
        download.execute();

        Log.e("refresh","---------------------------------------");

//        view.setRefreshing(false);
    }

    public class GetJson extends AsyncTask<Void, Void, ArrayList<Tasks>> {

        @Override
        protected void onPreExecute() {
            System.out.printf("aquii");
            load = ProgressDialog.show(getActivity(), "Por favor Aguarde ...", "Recuperando Informações do Servidor...");
        }

        @Override
        protected ArrayList<Tasks> doInBackground(Void... params) {
            Utils util = new Utils();

            return util.getInformacao("https://calm-reef-31190.herokuapp.com/api/tasks");
        }

        @Override
        protected void onPostExecute(ArrayList<Tasks> tasks){

            recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView); // atriu ao recycle view dentro do layot modelo_fragment
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));//seta o layot como Linear

            //cria o array com os itens do da lista(recycle view)
            Collections.sort(tasks);


            RecyclerView_Adapter adapter = new RecyclerView_Adapter(getActivity(), tasks);

            recyclerView.setAdapter(adapter);// set adapter on recyclerview
            load.dismiss();
        }

    }

    public class refresh extends AsyncTask<Void, Void, ArrayList<Tasks>> {


        @Override
        protected ArrayList<Tasks> doInBackground(Void... params) {
            Utils util = new Utils();

            return util.getInformacao("https://calm-reef-31190.herokuapp.com/api/tasks");
        }

        @Override
        protected void onPostExecute(ArrayList<Tasks> tasks){

            recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView); // atriu ao recycle view dentro do layot modelo_fragment
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));//seta o layot como Linear


            Collections.sort(tasks);

            RecyclerView_Adapter adapter = new RecyclerView_Adapter(getActivity(), tasks);

            recyclerView.setAdapter(adapter);// set adapter on recyclerview
            mSwipeRefreshLayout.setRefreshing(false);
        }

    }

}
