package sirios.buffett.view;

import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Pattern;

import sirios.buffett.R;
import sirios.buffett.controller.NetworkUtils;
import sirios.buffett.controller.Utils;
import sirios.buffett.model.Tasks;
import sirios.buffett.view.DummyFragment;
import sirios.buffett.view.MainActivity;
import sirios.buffett.view.RecyclerView_Adapter;

public class AdicionarDialog extends Dialog {

    private EditText name;
    private EditText detais;
    private EditText hora;

    private Tasks task;

    public AdicionarDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.adicionar_dialog);

        Button buttonAdicionar = (Button) findViewById(R.id.buttonAdcionar);
        buttonAdicionar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                adciona();
            }
        });

        Button buttonCancelar = (Button) findViewById(R.id.buttonCancelar);
        buttonCancelar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                cancelar();
            }
        });
        hora = (EditText) findViewById(R.id.textHora);
        hora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        hora.setText( selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

    }



    void adciona(){
        task = new Tasks();

        name = (EditText) findViewById(R.id.textName);
        detais = (EditText) findViewById(R.id.textDetails);

        task.setName(name.getText().toString());
        task.setDetails(detais.getText().toString());
        task.setHours(hora.getText().toString());

        Boolean b = Pattern.matches ("^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$", hora.getText());
        System.out.println(b);

        System.out.println("----------------------------------------------"+name.getText().toString());
        if(name.getText().toString().matches("") || detais.getText().toString().matches("")
                || hora.getText().toString().matches("")){
            mostraToast("Campos vazios");
        }else if(b == true){
            mostraToast("Horas invalida");
        }else{
            NetworkUtils.sendPost(task);
            mostraToast(task.getName().toString() + " Adicionado");
            this.dismiss();
        }
    }

    void cancelar(){
        this.dismiss();
    }

    void mostraToast(String texto){
        Toast.makeText(getContext(), texto, Toast.LENGTH_SHORT).show();


    }


}

