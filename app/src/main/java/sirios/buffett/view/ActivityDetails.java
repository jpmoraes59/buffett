package sirios.buffett.view;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import sirios.buffett.R;

/*
* tela de detalhes de cada tarefa
* */

public class ActivityDetails extends AppCompatActivity {

    public TextView campoTexto;
    public TextView campoHora;
    public TextView campoDetalhe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        campoTexto = (TextView) findViewById(R.id.textTitulo);
        campoDetalhe = (TextView) findViewById(R.id.textDetails);
        campoHora = (TextView) findViewById(R.id.textHora);


        String Nome = this.getIntent().getStringExtra("Nome");
        String Detalhes = this.getIntent().getStringExtra("Detalhes");
        String Data = this.getIntent().getStringExtra("Horas");
        campoTexto.setText(Nome);
        campoDetalhe.setText(Detalhes);
        campoHora.setText(Data);
        FloatingActionButton myFab = (FloatingActionButton) findViewById(R.id.botaoeditar);
        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new EditarDialog(ActivityDetails.this).show();

            }
        });
    }
}
