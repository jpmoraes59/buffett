package sirios.buffett.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import sirios.buffett.model.DemoViewHolder;
import sirios.buffett.R;
import sirios.buffett.controller.NetworkUtils;
import sirios.buffett.model.Tasks;

/*
 * monta as linhas do recycle view
 * */

public class RecyclerView_Adapter extends RecyclerView.Adapter<DemoViewHolder> {
    private ArrayList<Tasks> arrayList;
    private Context context;
    private AlertDialog alerta;


    public RecyclerView_Adapter(Context context, ArrayList<Tasks> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
        ArrayList<Tasks> sobra = new ArrayList<Tasks>();

        SimpleDateFormat dateFormat_hora = new SimpleDateFormat("HH:mm");
        Date data = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        Date data_atual = cal.getTime();
        String hora_atual = dateFormat_hora.format(data_atual);

        String passar = hora_atual.replace(":",".");
        Float horaSistema = Float.parseFloat(passar);
             for(Tasks t : this.arrayList) {
            System.out.println(t.getName());
            String passar2 = t.getHours().replace(":",".");
            Float horaTarefa = Float.parseFloat(passar2);
            Log.i("hora do sistema", horaTarefa.toString() +" > "+ horaSistema.toString() ); //
            if( horaTarefa  > horaSistema){
                Log.i("se é", hora_atual); //
                sobra.add(t);
            }
        }
        this.arrayList = sobra;
    }


    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);

    }


    @Override
    public void onBindViewHolder(DemoViewHolder holder, final int position) {
        final DemoViewHolder mainHolder = (DemoViewHolder) holder; //chama a clase que modela a linha
        mainHolder.text.setText(arrayList.get(position).getName()); //seta o titulo
        mainHolder.title.setText(arrayList.get(position).getHours()); //seta o texto

        mainHolder.text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();

                Intent intent = new Intent(context, ActivityDetails.class);
                String posicao = Integer.toString(position);

                Log.d("posicao", posicao);

                intent.putExtra("ID", arrayList.get(position).getId());
                intent.putExtra("Nome", arrayList.get(position).getName());
                intent.putExtra("Detalhes", arrayList.get(position).getDetails());
                intent.putExtra("Horas", arrayList.get(position).getHours());

                SharedPreferences.Editor editor = view.getContext().getSharedPreferences("ID", view.getContext().MODE_PRIVATE).edit();
                editor.putInt("id", arrayList.get(position).getId());
                editor.apply();

                context.startActivity(intent);
            }

        });


        mainHolder.text.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Titulo");
                //define a mensagem
                builder.setMessage("Apagar " + arrayList.get(position).getName());
                //define um botão como positivo
                builder.setPositiveButton("Apagar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        NetworkUtils.Apagar(arrayList.get(position));
                        Toast.makeText(context, arrayList.get(position).getName()+" Apagada", Toast.LENGTH_SHORT).show();
                    }
                });
                //define um botão como negativo.
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                });
                //cria o AlertDialog
                alerta = builder.create();
                //Exibe
                alerta.show();
                return true;
            }

        });


    }

    @Override
    public DemoViewHolder onCreateViewHolder(
            ViewGroup viewGroup, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

        ViewGroup mainGroup = (ViewGroup) mInflater.inflate(
                R.layout.modelo_row, viewGroup, false);
        DemoViewHolder mainHolder = new DemoViewHolder(mainGroup) {
            @Override
            public String toString() {
                return super.toString();
            }
        };


        return mainHolder;

    }

}
