package sirios.buffett.controller;


import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import sirios.buffett.model.Tasks;


public class Utils {

    public ArrayList<Tasks> getInformacao(String end) {
        String json;
        ArrayList<Tasks> retorno;
        json = NetworkUtils.getJSONFromAPI(end);
        Log.i("Resultado", json);
        retorno = parseJson(json);

        return retorno;
    }

    private ArrayList<Tasks> parseJson(String json) {
        try {

            ArrayList<Tasks> lista = new ArrayList<Tasks>();
            JSONObject jsonObj = new JSONObject(json);
            JSONArray array = jsonObj.getJSONArray("response");

//            JSONObject obj = objArray.getJSONObject("user");

            //Atribui os objetos que estão nas camadas mais altas
            for (int i = 0; i < array.length(); i++) {
                Tasks tasks = new Tasks();
                JSONObject objArray = array.getJSONObject(i);

                tasks.setId(objArray.getInt("id"));
                tasks.setName(objArray.getString("name"));
                tasks.setDetails(objArray.getString("details"));
                tasks.setHours(objArray.getString("hours"));
                tasks.setCreated_at(objArray.getString("created_at"));
                tasks.setUpdated_at(objArray.getString("updated_at"));

                lista.add(tasks);
                System.out.println(lista.get(i).getName());
            }

            return lista;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

}
