package sirios.buffett.controller;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import sirios.buffett.model.Tasks;


public class NetworkUtils {

    //Responsavel por carregar o Objeto JSON
    public static String getJSONFromAPI(String url) {
        String retorno = "";
        try {
            URL apiEnd = new URL(url);
            int codigoResposta;
            HttpURLConnection conexao;
            InputStream is;

            conexao = (HttpURLConnection) apiEnd.openConnection();
            conexao.setRequestMethod("GET");
            conexao.setReadTimeout(15000);
            conexao.setConnectTimeout(15000);
            conexao.connect();

            codigoResposta = conexao.getResponseCode();
            if (codigoResposta < HttpURLConnection.HTTP_BAD_REQUEST) {
                is = conexao.getInputStream();
            } else {
                is = conexao.getErrorStream();
            }

            retorno = converterInputStreamToString(is);
            is.close();
            conexao.disconnect();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return retorno;
    }

    private static String converterInputStreamToString(InputStream is) {
        StringBuffer buffer = new StringBuffer();
        try {
            BufferedReader br;
            String linha;

            br = new BufferedReader(new InputStreamReader(is));
            while ((linha = br.readLine()) != null) {
                buffer.append(linha);
            }

            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return buffer.toString();
    }

    public static void sendPost(final Tasks task) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL("https://calm-reef-31190.herokuapp.com/api/tasks");
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setDoOutput(true);
                    conn.setDoInput(true);

                    JSONObject jsonParam = new JSONObject();
                    jsonParam.put("name", task.getName());
                    jsonParam.put("details", task.getDetails());
                    jsonParam.put("hours", task.getHours());
                    Log.i("json", jsonParam.toString());


                    Log.i("JSON", jsonParam.toString());
                    DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                    //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                    os.writeBytes(jsonParam.toString());

                    os.flush();
                    os.close();

                    Log.i("STATUS", String.valueOf(conn.getResponseCode()));
                    Log.i("MSG", conn.getResponseMessage());

                    conn.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();
    }

    public static void Apagar(final Tasks task) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String nova = "https://calm-reef-31190.herokuapp.com/api/tasks/delete/" + task.getId();

                    URL apiEnd = new URL(nova);
                    int codigoResposta;
                    HttpURLConnection conexao;
                    InputStream is;

                    conexao = (HttpURLConnection) apiEnd.openConnection();
                    conexao.setRequestMethod("GET");
                    conexao.setReadTimeout(15000);
                    conexao.setConnectTimeout(15000);
                    conexao.connect();


                    codigoResposta = conexao.getResponseCode();
                    if (codigoResposta < HttpURLConnection.HTTP_BAD_REQUEST) {
                        is = conexao.getInputStream();
                    } else {
                        is = conexao.getErrorStream();
                    }
                    Log.i("STATUS", String.valueOf(conexao.getResponseCode()));
                    Log.i("MSG", conexao.getResponseMessage());
                    is.close();
                    conexao.disconnect();


                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();
    }

    public static void Editar(final Tasks task) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String idTask = String.valueOf(task.getId());
                    URL url = new URL("https://calm-reef-31190.herokuapp.com/api/tasks/" + idTask);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setDoOutput(true);
                    conn.setDoInput(true);

                    JSONObject jsonParam = new JSONObject();
                    jsonParam.put("name", task.getName());
                    jsonParam.put("details", task.getDetails());
                    jsonParam.put("hours", task.getHours());
                    Log.i("json", jsonParam.toString());


                    Log.i("JSON", jsonParam.toString());
                    DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                    //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                    os.writeBytes(jsonParam.toString());

                    os.flush();
                    os.close();

                    Log.i("STATUS", String.valueOf(conn.getResponseCode()));
                    Log.i("MSG", conn.getResponseMessage());

                    conn.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();
    }

}
