package sirios.buffett.model;

import android.support.annotation.NonNull;

import java.util.ArrayList;

public class Tasks implements Comparable<Tasks> {

    private int id;
    private String name;
    private String details;
    private String Hours;
    private String created_at;
    private String updated_at;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getHours() {
        return Hours;
    }

    public void setHours(String hours) {
        Hours = hours;
    }

    @Override
    public int compareTo(@NonNull Tasks tasks) {

        String passar = this.getHours().replace(":",".");
        Float teste = Float.parseFloat(passar);

        String passar2 = tasks.getHours().replace(":",".");
        Float teste2 = Float.parseFloat(passar2);

        if ( teste  < teste2) {
            return -1;
        }
        if ( teste  > teste2) {
            return 1;
        }
        return 0;
    }
}
