package sirios.buffett.model;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import sirios.buffett.R;

/*
* cria o modelo da linha no recicle view junto com o layout modelo_linha
*
* */

public class DemoViewHolder extends RecyclerView.ViewHolder {

    public TextView title;
    public TextView text;

    public DemoViewHolder(View view) {
        super(view);


        this.title = (TextView) view.findViewById(R.id.tituloLinha);
        this.text = (TextView) view.findViewById(R.id.textoLinha);

    }
}
